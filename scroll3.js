$(document).ready(function () {
    setTimeout(function () {
        $('.flow').addClass('animate-top');
    }, 1000);

    //header
    var waypointHeader = new Waypoint({
        element: $('header'),
        handler: function(direction) {
            if (direction === 'down') {
                $('header').addClass('tofix');
            } else if (direction === 'up') {
                $('header').removeClass('tofix');
            }
        },
        offset: -100
    })

    //blocks compaine
    var waypoint = new Waypoint({
        element: document.getElementById('down'),
        handler: function(direction) {
            if (direction === 'down') {
                $('.flow').addClass('hook');
            } else if (direction === 'up') {
                $('.flow').removeClass('hook');
            }
        },
        offset: 600
    })
});

