$("document").ready(function () {

  var filterSlider = document.getElementById('filter-slider');

  noUiSlider.create(filterSlider, {
    start: [10, 400],
    connect: true,
    range: {
      'min': 10,
      'max': 400
    },
    //format: wNumb({
    //  decimals: 0,
    //  //thousand: '.',
    //  prefix: '$',
    //})
  });


  var inputMin = document.getElementById('min1');
  var inputMax = document.getElementById('max1');


  filterSlider.noUiSlider.on('update', function (values, handle) {

    var value = values[handle];
    //min1.value = values[handle];
    //max1.value = values[handle];


    if (handle) {
      inputMax.value = '$' + Math.round(value);
    } else {
      inputMin.value = '$' + Math.round(value);
    }
  });

  inputMax.addEventListener('change', function () {
    filterSlider.noUiSlider.set([null, this.value]);
  });

  inputMin.addEventListener('change', function () {
    filterSlider.noUiSlider.set([this.value, null]);
  });



 // min1.addEventListener('change', function () {
 //   sliderFormat.noUiSlider.set(this.value);
 // });
//
  //var sliderFormat = document.getElementById('slider-format');
//
  //noUiSlider.create(sliderFormat, {
  //  start: [ 20, 800],
  //  //step: 1000,
  //  range: {
  //    'min': [ 20 ],
  //    'max': [ 8000 ]
  //  },
  //  format: wNumb({
  //    decimals: 0,
  //    //thousand: '.',
  //    prefix: '$',
  //  })
  //});
  //var inputFormat = document.getElementById('input-format');
//
  //sliderFormat.noUiSlider.on('update', function( values, handle ) {
  //  inputFormat.value = values[handle];
  //});
//
  //inputFormat.addEventListener('change', function(){
  //  sliderFormat.noUiSlider.set(this.value);
  //});
});
